/*
 * ePower2 Table Calculator and Display Function (TCD) Definitions
 */

#include "ePower2.h"

circuit_input_info data;
circuit_input_info * dptr = &data;

void set_voltage (power_calc * ptr, double V)
{
    ptr->V = V;
}
void set_resistance (power_calc * ptr, double R)
{
    ptr->R = R;
}
void set_power (power_calc * ptr, double P)
{
    ptr->P = P;
}

double get_voltage (power_calc * ptr)
{
    return ptr->V;
}
double get_resistance (power_calc * ptr)
{
    return ptr->R;
}
double get_power (power_calc * ptr)
{
    return ptr->P;
}

/*
 * Power calculator function for table display.
 *
 * INPUT: voltage = voltage applied to electrical circuit
 *        resistance = resistance for each resistor in the electrical circuit
 *        N = number of resistors in series/parallel
 *        type = SERIES (circuit in series) or PARALLEL (circuit in parallel)
 *
 * Formula used for both SERIES and PARALLEL: P_circuit = V * (V / R_eq)
 * OPTIMIZATION added: let P = V * (V / R), then:
 *      for SERIES: P_circuit = P_series = P / N
 *      for PARALLEL: P_circuit = P_parallel = P * N
 *
 * RETURNS: Electrical power of the circuit
 */
double calc_power(double voltage, double resistance, circuit_input_info * dptr)
{
    double P = voltage * (voltage / resistance);
    double P_series = P / dptr->N;
    double P_parallel = P * dptr->N;

    if (dptr->type == SERIES) {
        return P_series;
    }
    else {
        return P_parallel;
    }
}

// Macro to calculate the minimum of two values
#define min(a, b) (((a) < (b)) ? (a) : (b))

/*
 * Power Calculator and Table Display Master Function for user-specified
 * voltage and resistance ranges.
 */
void circuit_power_table(char * filename)
{
    // Step 1:  get all the input values

    dptr->Vmin = volt_min();
    dptr->Vmax = volt_max();
    if (dptr->Vmin >= dptr->Vmax) {
        printf("ERROR, Vmax = %g must be greater than Vmin = %g\n\n",
               dptr->Vmax, dptr->Vmin);
        exit(1);
    }
    dptr->deltaV = volt_delta();

    dptr->Rmin = resistance_min();
    dptr->Rmax = resistance_max();
    if (dptr->Rmin >= dptr->Rmax) {
        printf("ERROR, Rmax = %g must be greater than Rmin = %g\n\n",
               dptr->Rmax, dptr->Rmin);
        exit(1);
    }
    dptr->deltaR = resistance_delta();

    dptr->N = resistor_quantity();
    dptr->type = circuit_type();

    printf("\n");

    // Step 2: V_num is the number of voltage values that will be displayed
    int V_num = (int)((dptr->Vmax - dptr->Vmin) / dptr->deltaV) + 1;
            //'()' converts result to integer, this is known as casting

    // Step 3: R_num is the number of resistance values that will be displayed
    int R_num = (int)((dptr->Rmax - dptr->Rmin) / dptr->deltaR) + 1;

    // Step 4: allocate memory for all (R,V,P) elements
    power_calc * db_start = (power_calc *)malloc(R_num * V_num *
                                             sizeof(power_calc));
    if (db_start == NULL) {
        printf("\nFailed to allocate memory.\n\n");
        exit(1);
    }

    // Step 5: define here a maximum voltages per line to be displayed
    int max_per_line = 6;

    // Step 6: table_num refers to how many tables will be displayed
    int table_num = (V_num-1)/max_per_line + 1;

    if (table_num > 100) {
        printf("ERROR. Revise input data, too many values to calculate."
               "\ntable_num = %d, which is greater than the maximum allowed "
               "(100).", table_num);
        exit(1);
    }

    // Step 7: fill in the db_start memory with all (R, V, P) values
    power_calc * db = db_start;
    for (int i = 0; i < table_num; i++) {
        // setting R, V, P values for one table
        double R = dptr->Rmin;

        // only for the last table, we might set less than max_per_line V values,
        // if previous tables already set each of them max_per_line V values,
        // hence the 'min' below
        int V_num_per_line = min(max_per_line, V_num - i * max_per_line);
        for (int j = 0; j < R_num; j++) {
            // setting R, V, P for one row in a table
            double V = dptr->Vmin + i * max_per_line * dptr->deltaV;
            for (int k = 0; k < V_num_per_line; k++) {
                // setting R, V, P for one row/column in a table
                set_voltage(db, V);
                set_resistance(db, R);
                set_power(db, calc_power(V, R, dptr));
                V += dptr->deltaV;
                db++;
            }
            R += dptr->deltaR;
        }
    }

    // Step 8: display (R, V, P) values from db_start memory directly on the
    //         command line window and in a CSV file.
    db = db_start;

    FILE * fp;
    fp = fopen(filename, "w+");

    for (int i = 0; i < table_num; i++) {
        printf("\n");
        print_header(fp, get_voltage(db), dptr, max_per_line);

        int V_num_per_line = min(max_per_line, V_num - i * max_per_line);
        line_out(&db, V_num_per_line, R_num, fp);
        fprintf(fp, "\n\n");
    }

    fclose(fp);
    printf("\n'%s' file created with the above values.\n\n", filename);
    free(db_start);
}

/*
 * ePower2 Input Validator Function Definitions
 */

#include "ePower2.h"

// The program will take values within a given range
#define MIN_VAL 0.1
#define MAX_VAL 10000.0
#define MAX_INTVAL 100

/*
 * Real number reader and validator function
 * Validation is done for:
 * 1) sign check (exclusion of negative values)
 * 2) exclusion of zero value
 * 3) exclusion of non-numerical characters
 *
 * Returns:
 * SUCCESS (0) if parameter is correct, ERROR (1) otherwise
 * If SUCCESS, real number is stored in address of 'number'
 */
int real_number_sign_check(double * number)
{
    double value;
    char newline;

    // Expect with scanf a real number followed only by a newline
    int ret = scanf("%lg%c", &value, &newline);

    /*
     * If number of input paramters is not 2, signal ERROR
     * We expect only two parameters - a real number and a newline
     */
    if (ret != 2) {
        printf("ERROR. ");
        return ERROR;
    }

    /*
     * If parameter following real number is not a newline character, signal
     * ERROR
     */
    if (newline != '\n') {
        printf("ERROR, unexpected character or characters.\n");
        return ERROR;
    }

    //If real number is negative or equal to zero, signal ERROR
    if (value < 0) {
        printf("ERROR, cannot relay your value as it is negative.\n");
        return ERROR;
    }
    else if (value == 0) {
        printf("ERROR, I cannot relay your value as I can say nothing. ;)\n");
        return ERROR;
    }

    if (value < MIN_VAL || value > MAX_VAL) {
        printf("ERROR, can not accept the value. Please revise."
               "\nInput value: %g is outside accepted range of: "
               "%g - %g\n\n", value, MIN_VAL, MAX_VAL);
        exit(1);
    }

    // Real number successfully introduced. Store it in the address of number
    *number = value;

    return SUCCESS;
}

/*
 * Positive integer number reader and validator function
 * Validation is done for:
 * 1) exclusion of zero value
 * 2) exclusion of non-numerical characters
 *
 * Returns:
 * SUCCESS (0) if parameter is correct, ERROR (1) otherwise.
 * If SUCCESS, positive integer number is stored in address of 'number'
 */
int int_check(unsigned int * number)
{
    unsigned int intvalue;
    char newline;

    // Expect with scanf a positive integer number followed only by a newline
    int ret = scanf("%u%c", &intvalue, &newline);

    /*
     * If number of input paramters is not 2, signal ERROR
     * We expect only two parameters - a positive integer number and a newline
     */
    if (ret != 2) {
        printf("ERROR. ");
        return ERROR;
    }

    /*
     * If parameter following positive integer number is not a newline
     * character, signal ERROR
     */
    if (newline != '\n') {
        printf("ERROR, unexpected character or characters.\n");
        return ERROR;
    }

    //If positive integer number is equal to zero, signal ERROR
    if (intvalue == 0) {
        printf("Sorry, I cannot relay your value as I can say nothing. ;)\n");
        return ERROR;
    }

    if (intvalue > MAX_INTVAL) {
        printf("ERROR, can not accept the value. Please revise."
               "\nInput value: %d is greater than maximum allowed value of : "
               "%g\n\n", intvalue, MAX_VAL);
        exit(1);
    }

    /*
     * Positive integer number successfully introduced
     * Store it in the address of 'number'
     */
    *number = intvalue;

    return SUCCESS;
}

/*
 * Character reader and validator function
 * Validation is done for:
 * - 'S', 's', 'P', 'p', '\n'
 * - '\n' is interpreted in the same way as 'S' and 's' (SERIES circuit)
 *
 * Returns:
 * SUCCESS (0) if character is one of the above, ERROR (1) otherwise
 * If SUCCESS, circuit type (SERIES/PARALLEL) is stored in address of 'type'
 */
int SP_char_check(int * type)
{
    char chrvalue;

    // Expect with scanf only one character
    int ret = scanf("%c", &chrvalue);

    // If more than one character introduced, signal ERROR
    if (ret != 1) {
        printf("ERROR. ");
        return ERROR;
    }

    /*
     * Debug statement to understand the value of the input character in
     * hexadecimal. It has been commented out once the error has been found
     * (two newlines needed initially to accept default case of '\n').
     */
        //printf("DEBUG: chrvalue = 0x%02x\n", chrvalue);


    // If no letter is given, SERIES is the default case triggered by '\n'.
    if (chrvalue == 'S' || chrvalue == 's' || chrvalue == '\n') {
        *type = SERIES;
    }
    else if (chrvalue == 'P' || chrvalue == 'p') {
        *type = PARALLEL;
    }
    else {
        printf("ERROR. ");
        return ERROR;
    }

    return SUCCESS;
}

/*
 * Character reader and validator function.
 * Validation is done for:
 * - 'Y', 'y', 'N', 'n', '\n'
 * - '\n' is interpreted in the same way as 'Y' and 'y' (yes)
 *
 * Returns:
 * SUCCESS (0) if character is one of the above, ERROR (1) otherwise.
 * If SUCCESS, circuit type (SERIES/PARALLEL) is stored in address of 'type'.
 */
int YN_char_check(int * answer)
{
    char chrvalue;

    // Expect with scanf only one character.
    int ret = scanf("%c", &chrvalue);

    // If more than one character introduced, signal ERROR.
    if (ret != 1) {
        printf("Sorry, I did not understand that response. \n\n");
        return ERROR;
    }

    /*
     * Debug statement to understand the value of the input character in
     * hexadecimal. It has been commented out once the error has been found
     * (two newlines needed initially to accept default case of '\n').
     */
        //printf("DEBUG: chrvalue = 0x%02x\n", chrvalue);


    // If no letter is given, YES is the default case triggered by '\n'.
    if (chrvalue == 'Y' || chrvalue == 'y' || chrvalue == '\n') {
        *answer = YES;
    }
    else if (chrvalue == 'N' || chrvalue == 'n') {
        *answer = NO;
    }
    else {
        printf("Sorry, I did not understand that response. \n\n");
        return ERROR;
    }

    return SUCCESS;
}

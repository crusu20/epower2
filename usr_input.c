/*
 * ePower2 User Input Helper Function Definitions
 */

#include "ePower2.h"

/*
 * Get from the console input the voltage, perform sanity check of the
 * input value.
 *
 * This function decides whether or not to abort the program based on the
 * return code of the real number reader and validator function.
 *
 * Returns:
 * 'voltage' value
 */
double volt()
{
    // Invite user to input data for the voltage.
    printf("\nEnter voltage value (Volts): ");

    double voltage;
    int ret = real_number_sign_check(&voltage);

    /*
     * The variable ret is an error code: if zero, all good. Otherwise, error
     * exists in the function real_number_sign_check which leads to the
     * abortion of the program exit(1).
     */
    if (ret != 0) {
        printf("Can not proceed with calculation, please revise choice "
               "of input.\n\n");
        exit(1);
    }
    return voltage;
}

/*
 * Get from the console input the resistance, perform sanity check of the
 * input value.
 *
 * This function decides whether or not to abort the program based on the
 * return code of the real number reader and validator function.
 *
 * Returns:
 * 'resistance' value
 */
double resistance()
{
    // Invite user to input data for resistance.
    printf("\nEnter resistor value (Ohms): ");

    double resistance;
    int ret = real_number_sign_check(&resistance);

    /*
     * The variable ret is an error code: if zero, all good. Otherwise, error
     * exists in the function real_number_sign_check which leads to the abortion of the
     * program exit(1).
     */
    if (ret != 0) {
        printf("Can not proceed with calculation, please revise choice "
               "of input.\n\n");
        exit(1);
    }
    return resistance;
}

/*
 * Get from the console input the resistor quantity, perform sanity check of the
 * input value.
 *
 * This function decides whether or not to abort the program based on the
 * return code of the positive integer number reader and validator function.
 *
 * Returns:
 * resistor quantity 'N' value
 */
unsigned int resistor_quantity()
{
    // Invite user to input data for resistor quantity.
    printf("\nEnter resistor quantity N: ");

    unsigned int N;
    int ret = int_check(&N);

    /*
     * The variable ret is an error code: if zero, all good. Otherwise, error
     * exists in the function int_check which leads to the abortion of the
     * program exit(1).
     */
    if (ret != 0) {
        printf("Can not proceed with calculation, please revise choice "
               "of input for resistor quantity.\n\n");
        exit(1);
    }
    return N;
}

/*
 * Get from the console input the circuit type, perform sanity check of the
 * input value.
 *
 * This function decides whether or not to abort the program based on the
 * return code of the character reader and validator function.
 *
 * Returns:
 * circuit 'type' value
 */
int circuit_type()
{
    // Invite user to input data for circuit type.
    printf("\nIs the circuit Series (default) or Parallel? (S/P): ");

    int type;

    int ret = SP_char_check(&type);

    /*
     * The variable ret is an error code: if zero, all good. Otherwise, error
     * exists in the function SP_char_check which leads to the abortion of the
     * program exit(1).
     */
    if (ret == 0) {
        if (type == SERIES) {
            printf("\nAll good, proceeding with calculation of electrical "
                   "power for resistors arranged in series.\n");
        }
        else {
            printf("\nAll good, proceeding with calculation of electrical "
                   "power for resistors arranged in parallel.\n");
        }
    }
    else {
        printf("Can not proceed with calculation, please revise choice "
               "of input for circuit type.\n\n");
        exit(1);
    }

    return type;
}

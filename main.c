/*
 *    PROBLEM DEFINITION:
 *    Calculate and display the electrical power consumed by a circuit
 *    when a voltage V is applied to it, considering two cases:
 *        a) The circuit is formed by N identical resistors in series
 *        b) The circuit is formed by N identical resistors in parallel
 *
 *    Calculate and display the equivalent resistance of the circuit.
 */

// Include user-defined header file containing symbolic constant definitions
// and function declarations.
#include "ePower2.h"

/*
 * Main function to select power calculation style using the input data
 * from the user:
 * - voltage (V)
 * - resistance (R)
 * - resistor quantity (N)
 * - circuit type (SERIES/PARALLEL)
 */

// Main function takes an argument count and an argument vector.
// See filename sanitation below for details on how these are used. 
int main(int argc, char ** argv)
{
    // Invite user to select calculation option
    printf("\nCalculate a range of power values? (Y/N) : ");

    int answer;

    unsigned int ret = YN_char_check(&answer);

    /*
     * The variable ret is an error code: if zero, all good. Otherwise, error
     * exists in the function YN_char_check which leads to the abortion of the
     * program exit(1).
     */
    if (ret == 0) {
        if (answer == YES) {
            // Prior to proceeding with calculation of power range,
            // sanitation of filename input must be performed.
            // The main function will take only an argument count of 2 :
                            // 1) Program name
                            // 2) File name
            if (argc != 2) {
                printf("Incorrect syntax. Please run the following command : "
                       "ePower2_exec <filename_of_your_choice>.csv\n\n");
                exit(1);
            }
            char filename[NAME_SIZE];
            // Definition of variable for the length of the second element
            // in the argument vector.
            int len = (int)strlen(argv[1]);
            if (len >= NAME_SIZE) {
                printf("Incorrect filename length of %d, it must be less than "
                       "%d.\n\n", len, NAME_SIZE);
                exit(1);
            }
            strcpy(filename, argv[1]);
            if (strcmp(&filename[len - 4], ".csv") != 0) {
                printf("Incorrect filename format."
                       "Please use the extension '.csv'.\n\n");
                exit(1);
            }
            FILE * file;
            if ((file = fopen(filename, "r"))) {
                fclose(file);
                printf("File '%s' already exists. Not possible to overwrite "
                       "in ePower2.\n\n", filename);
                exit(1);
            }

            printf("All good, proceeding with calculation of power range. "
                   "\n");
            circuit_power_table(filename);

        }
        else {
            printf("All good, proceeding with calculation of a single power "
                   "value.\n");
            single_circuit_power();
        }
    }
    else {
        exit(1);
    }

    return SUCCESS;
}

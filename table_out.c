// File for Table Output on the command line window and in a CSV file.

#include "ePower2.h"

// Header Function for table display of labels and voltage values
void print_header(FILE * fp, double V, circuit_input_info * dptr,
                  int max_per_line)
{
    int columns = 1; // There is at least one column for resistance values
    double V_temp = V;
    for (int x = 0; x < max_per_line; x++) {
        columns++;
        V_temp += dptr->deltaV;
        if (V_temp > dptr->Vmax) {
         break;
        }
    }

    // Double line divider
    printf("\n");
    for (int x = 0; x < columns; x++) {
        printf("-----------------");
    }
    printf("\n");
    for (int x = 0; x < columns; x++) {
        printf("-----------------");
    }

    // Display labels for horizontal and vertical axes of each table
    fprintf(fp, "Resistance (Ohms), Voltages (V)\n,");
    printf("\nResistance     | Voltage (V)    |");
    for (int x = 0; x < (columns - 3); x++) {
        printf("                 ");
    }
    if (columns >= 3) {
        printf("                |");
    }
    printf("\n");
    printf("(Ohms)         | ");

    // Display voltage header values within range specified by user
    V_temp = V;
    for (int a = 0; a < max_per_line; a++) {
        // 13.3 refers to "use at least 13 chr out of which min 3 are dec."
        // If more than 13 chr are needed, then use more than 13 chr.
        // printf prints 1 space, 13 chr, 1 space, 1 vertical bar, 1 space
                         //total of 17 chr.
        printf(" %13.3f | ", V_temp);
        fprintf(fp, "%13.3f,", V_temp);
        V_temp += dptr->deltaV;
        // if V is greater than the maximum value specified by user input,
        // stop printing.
        if (V_temp > dptr->Vmax) {
            break;
        }
    }

    // Single line divider
    printf("\n");
    for (int x = 0; x < columns; x++) {
        printf("-----------------");
    }
    printf("\n");
    fprintf(fp, "\n");
}

// Function for resistance and power value output
void line_out(power_calc **db, int V_num_per_line, int R_num, FILE * fp) {
    // Number of lines in the table depends on user input for resistance range
    for (int j = 0; j < R_num; j++) {
        fprintf(stdout, " %13.3f | ", get_resistance(*db));
        fprintf(fp, "%13.3f,", get_resistance(*db));
        // Display 6 power values per line
        for (int k = 0; k < V_num_per_line; k++) {
            fprintf(stdout, " %13.3f | ", get_power(*db));
            fprintf(fp, "%13.3f,", get_power(*db));
            (*db)++;
        }
        printf("\n");
        fprintf(fp, "\n");
    }
}

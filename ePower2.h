/*
 * Header File containing ePower2 symbolic constant definitions and function
 * declarations.
 */

// Opening of once-only header file (if needed)
 // #ifndef EPOWER2
 // #define EPOWER2

// Include system header files
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SERIES 0
#define PARALLEL 1
#define SUCCESS 0
#define ERROR 1
#define YES 0
#define NO 1
#define NAME_SIZE 100

typedef struct {
    double Vmin;
    double Vmax;
    double deltaV;
    double Rmin;
    double Rmax;
    double deltaR;
    unsigned int N;
    int type;
} circuit_input_info;

typedef struct {
    double V;
    double R;
    double P;
} power_calc;

int real_number_sign_check(double * number);
int int_check(unsigned int * number);
int SP_char_check(int * type);
int YN_char_check(int * answer);

double volt();
double resistance();
unsigned int resistor_quantity();
int circuit_type();

double volt_min();
double volt_max();
double volt_delta();
double resistance_min();
double resistance_max();
double resistance_delta();

void set_voltage (power_calc * ptr, double V);
void set_resistance (power_calc * ptr, double R);
void set_power (power_calc * ptr, double P);

double get_voltage (power_calc * ptr);
double get_resistance (power_calc * ptr);
double get_power (power_calc * ptr);

double calc_power(double voltage, double resistance, circuit_input_info * dptr);
void print_header(FILE * fp, double V, circuit_input_info * dptr,
                  int max_per_line);
void circuit_power_table(char * filename);
void line_out(power_calc **db, int V_num_per_line, int R_num, FILE * fp);

double R_equivalent_calc(int type, double resistance, int N);
void single_circuit_power();

// Closing of once-only header file (if needed)
 // #endif

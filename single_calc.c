/*
 * ePower2 Single Value Calculator Function Definitions
 */

#include "ePower2.h"

/*
 * Calculator function for the equivalent resistance of the circuit.
 *
 * Input:
 *  type = circuit type (SERIES/PARALLEL)
 *  resistance = resistor value (R)
 *  N = resistor quantity
 *
 * Returns:
 *  equivalent resistance 'R_eq'
 *
 * For SERIES, R_eq = R * N
 * For PARALLEL, R_eq = R / N
 */
double R_equivalent_calc(int type, double resistance, int N)
{
    double R_eq;

    if (type == SERIES) {
        R_eq = resistance * N;
    }
    else {
        R_eq = resistance / N;
    }

    return R_eq;
}

/*
 * Power calculator function.
 *
 * INPUT:
 *
 * Formula used for both SERIES and PARALLEL: P_circuit = V * (V / R_eq)
 * OPTIMIZATION added: let P = V * (V / R), then:
 *      for SERIES: P_circuit = P_series = P / N
 *      for PARALLEL: P_circuit = P_parallel = P * N
 */
void single_circuit_power()
{
    double V = volt();
    double R = resistance();
    unsigned int N = resistor_quantity();
    int type = circuit_type();
    double R_eq = R_equivalent_calc(type, R, N);

    double P = V * (V / R);
    double P_series = P / N;
    double P_parallel = P * N;

    if (type == SERIES) {
        printf("\nThe total electical power consumed by this series circuit "
               "is: %g Watts.\nThe equivalent resistance is %g Ohms.\n",
               P_series, R_eq);
    }
    else {
        printf("\nThe total electical power consumed by this parallel circuit "
               "is: %g Watts.\nThe equivalent resistance is %g Ohms.\n",
               P_parallel, R_eq);
    }

    printf("\n");
}

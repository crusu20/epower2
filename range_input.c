/*
 * ePower2 Range Value Input Function Definitions
 */

#include "ePower2.h"

double volt_min()
{
    printf("\nMinimum Voltage Value: ");

    double Vmin = volt();

    return Vmin;
}

double volt_max()
{
    printf("\nMaximum Voltage Value: ");

    double Vmax = volt();

    return Vmax;
}

double volt_delta()
{
    printf("\nVoltage Increment Value: ");

    double deltaV = volt();

    return deltaV;
}

double resistance_min()
{
    printf("\nMinimum Resistance Value: ");

    double Rmin = resistance();

    return Rmin;
}

double resistance_max()
{
    printf("\nMaximum Resistance Value: ");

    double Rmax = resistance();

    return Rmax;
}

double resistance_delta()
{
    printf("\nResistance Increment Value: ");

    double deltaR = resistance();

    return deltaR;
}
